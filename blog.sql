-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 07-03-2019 a las 15:39:23
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `blog`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `keywords`
--

CREATE TABLE `keywords` (
  `id_keywords` int(11) NOT NULL,
  `keyword` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `keywords`
--

INSERT INTO `keywords` (`id_keywords`, `keyword`) VALUES
(65, 'aventuras'),
(241, 'cuquis'),
(2, 'deportes'),
(261, 'Desarrollo Web'),
(255, 'eSports'),
(260, 'guacamole'),
(120, 'Juan'),
(162, 'laguna'),
(257, 'Motor'),
(1, 'noticias'),
(122, 'Parapa'),
(242, 'pepe'),
(3, 'videojuegos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `keywordsNews`
--

CREATE TABLE `keywordsNews` (
  `id_noticia` int(11) NOT NULL,
  `id_keyword` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `keywordsNews`
--

INSERT INTO `keywordsNews` (`id_noticia`, `id_keyword`) VALUES
(19, 1),
(21, 3),
(64, 2),
(64, 242),
(65, 1),
(65, 2),
(65, 3),
(65, 65),
(66, 65),
(66, 242),
(66, 255),
(67, 2),
(67, 3),
(67, 65),
(67, 241),
(68, 65),
(68, 241),
(68, 242),
(68, 260),
(90, 65);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `id_noticia` int(11) NOT NULL,
  `autor` varchar(30) NOT NULL,
  `editor` varchar(30) NOT NULL,
  `id_seccion` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `subtitulo` varchar(100) NOT NULL,
  `texto` varchar(5000) NOT NULL,
  `imagen` varchar(500) NOT NULL,
  `fecha_creacion` date NOT NULL,
  `fecha_modificacion` date NOT NULL,
  `fecha_publicacion` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id_noticia`, `autor`, `editor`, `id_seccion`, `titulo`, `subtitulo`, `texto`, `imagen`, `fecha_creacion`, `fecha_modificacion`, `fecha_publicacion`) VALUES
(19, 'lleno', 'editor', 1, 'Â¿Porque usamos las redes sociales?', 'Â¿Alguna vez te has preguntado porque utilizas las redes sociales?', '\r\n            \r\n            <div>\r\n            \r\n            A lo que llamamos redes sociales, no es mÃ¡s que la versiÃ³n digital de nuestras relaciones con otras personas, hemos encontrado en ellas una manera de expresarnos libremente y en la que siempre habrÃ¡ alguien para escucharnos, es difÃ­cil estar en Twitter, Pinterest o Instagram y no conseguir un seguidor o un me gusta de algo y cuando esta acciÃ³n se realiza nos invade una alegrÃ­a con un pensamiento inconsciente \"ey, esto mola :D\" y queremos mÃ¡s, porque en el fondo todos queremos en mayor o menor medida sentir que gustamos a otras personas, que lo que hacemos le interesa a alguien y no queremos sentirnos solos.\r\n\r\nMarcas en las redes sociales\r\n\r\nLa cuestiÃ³n no es porque utilizamos las redes sociales en nuestro entorno privado social, eso esta bastante claro, sino porque las utilizamos para empresas y que beneficios nos aporta. <br></div><div><br></div><div>Se utilizan las redes sociales sobre Marcas o empresas, porque dicen que es lo que tenemos que hacer, es lo mÃ¡s guay y la mejor manera de promocionarse a dÃ­a de hoy a coste 0, aunque realmente no sea asÃ­, es cierto que hay miles de casos de Ã©xito en las redes sociales donde empresas o personas han triunfado como la Coca Cola, pero si han triunfado es porque destacaban en algo y en las redes sociales funcionan las cosas que aportan algÃºn tipo de emociÃ³n o sentimiento, de ahÃ­ que los vÃ­deos de gatos se lo mÃ¡s visto. Â¿Que sentido tiene estar mandando mensaje a ningÃºn sitio esperando a que alguien por casualidad lo lea y diga \"esto me gusta, lo voy a compartir\"? Si alguien te dice que la manera de promocionar tu marca y conseguir clientes es escribir en un muro esperando a que alguien lo lea y comparta ese mensaje, tu le dirÃ­as que es tonto, que para que vas a hacer eso, sino hay nadie que lo pueda leer, porque no hay que olvidar que cuando empiezas en una red social, estas solo y escribes solo para ti.\r\n\r\nSi vas a estar en las redes sociales, tienes que estar por alguna razÃ³n y para aportar algo, si vas a abrirte una cuenta de Facebook o de Twitter en la que no vas a decir nada y solo lo haces porque te dicen que tienes que estar ahÃ­, mal asunto, sino vas a establecer contactos, clientes o ventas. Mejor dedica tu tiempo a otras cosas o invierte en publicidad, que es donde mÃ¡s puntos tienes para conseguir tu propÃ³sito y sobre todo, si lo haces, hazlo a gusto, dale tu personalidad y disfruta con ello, porque si solo lo haces por hacer, no trasmitirÃ¡s nada.                        </div>                        ', 'http://localhost/blog/view/image/noticias/lleno/post/1366_2000.jpg', '2019-01-17', '2019-02-28', '0000-00-00'),
(20, 'lleno', 'editor', 2, 'Ser tacaÃ±o es gratis y en este paÃ­s mÃ¡s', '', '\r\n            Si hay algo que siempre me sorprende es la condiciÃ³n humana, aunque no en todos los aspectos, sino en algunos. En EspaÃ±a, en especial, como ya he dicho otras muchas veces, es el paÃ­s del todo gratis, se va con la mentalidad de que todo se puede tener sin tener que pagar, puede ser la mÃºsica, pelÃ­culas, videojuegos o lo que sea.\r\n\r\nComo seguro que habÃ©is leÃ­do en otros medios,&nbsp;Whatsapp Android tiene mucha competencia, las mÃ¡s cercanas son Line, WeChat, BBmesenger o la mÃ¡s reciente apariciÃ³n que es Telegram.\r\n\r\nLo que me sorprende es ver como en EspaÃ±a estas plataformas que son gratuitas tienen una penetraciÃ³n de mercado muy grande, mÃ¡s que en otro paÃ­ses y todo porque es gratis, lo que mÃ¡s me sorprende todavÃ­a es que la gente se pase a otras plataformas por no pagar 79 centimos de euro, cuando hace unos aÃ±os varios SMS o mÃ¡s bien un solo MMS, podia acabar valiendo mÃ¡s dinero que un pago anual y se pagaba sin mÃ¡s.\r\n\r\nMe sorprende ver como las personas pueden llegar a tener esa mentalidad de no pagar por algo como el Whatsapp que solo vale 0,79â‚¬, pero en cambio se dejan grandes cantidades de dinero en cosas menos prescindibles como el alcohol, el tabaco, drogas, cenas en restaurantes, gimnasios a los que no vamos o los mismos smartphones que no son nada econÃ³micos.\r\n\r\nNo hablamos de grandes pagos, sino 0,79â‚¬ por una aplicaciÃ³n que nos ha ahorrado cientos de euros al aÃ±o.\r\n\r\n            ', 'http://localhost/blog/view/image/noticias/lleno/post/waypaut covers.png', '2019-01-17', '2019-02-27', '2019-02-27'),
(21, 'lleno', 'editor', 6, 'Â¿Donde estarÃ¡s dentro de un aÃ±o, y Facebook?', '', '\r\n            \r\n            \r\n            \r\n            \r\n            Durante toda la semana pasada y parte de esta he estado leyendo en decenas de sitios la misma noticia sobre que Facebook para dentro de unos aÃ±os habrÃ¡ perdido el 80% de sus usuarios y como ellos rebatieron basandose en los mismos estudios para decir que la universidad que habÃ­a hecho dicho estudio se quedarÃ­a sin estudiantes en un determinado plazo.\r\n\r\nEn primer lugar, es cierto que Facebook esta perdiendo popularidad, pero eso no significa que vaya a morir dentro de unos aÃ±os, Facebook es tan solo un medio de comunicaciÃ³n muy arraigado en las vidas de todos, y aunque se le haga menos caso que hace aÃ±os y la publicidad nos bombardÃ© de una manera muy bestia, actualmente es como el Whatsapp, un medio que nos ayuda a comunicarnos con las personas de nuestro entorno.\r\n\r\nÂ¿Alguna vez te has planteado como serÃ¡ tu vida cetro de 10 aÃ±os? o Â¿donde estarÃ¡ tu negocio dentro de 1 aÃ±o?\r\n\r\nSeguro que en ningÃºn momento querrÃ¡s pensar que todo se vaya a acabar, pues eso mismo le pasa a Facebook tambiÃ©n y le ha pasado a gran multitud de empresas. No se cuantas veces escuche que el Messenger se iba a convertir de pago, que esta vez si, hasta del Facebook lo escuche y todos esos rumores siempre estarÃ¡n ahÃ­, hasta que al final un dÃ­a alguno se hace realidad.\r\n\r\nLa Ãºnica manera de que no pase, es evolucionar, escuchar a tu publico y darle lo que necesita, tratar a los usuarios como personas y no dejar que la codicia del dinero te traicione.                                                            ', 'http://localhost/blog/view/image/noticias/lleno/post/1537195975-85667-zoom-500x600.jpg', '2019-01-17', '2019-02-27', '2019-02-27'),
(35, 'lleno', '', 4, 'Marmotasa tuti plen', '', '\r\n            \r\n            \r\n            \r\n        <p>Hola Marmotilla, como va todo, ahora tengo mi propio blog para hacer mis propias publicaciones, ueee jajaja lol</p>\r\n\r\n                                            ', 'http://localhost/blog/view/image/noticias/lleno/post/51CO_2BY4bdPL_1024x1024.jpg', '2019-02-01', '2019-02-24', '0000-00-00'),
(64, 'lleno', '', 7, 'noticia publicada por el administrador', 'Subtitulo', '\r\n            \r\n            \r\n            \r\n            \r\n            \r\n            \r\n            Aqui tenemos el texto que vamos a incluir en la siguiente publicacion con lo que sea                        ', 'http://localhost/blog/view/image/noticias/lleno/post/1366_2000.jpg', '2019-02-06', '2019-02-24', '0000-00-00'),
(65, 'lleno', 'editor', 2, 'pelocho calimocho', '', '<p style=\"margin-right: auto; margin-bottom: 17.68px; margin-left: auto; font-size: 19px; max-width: 696px; caret-color: rgb(51, 51, 51); color: rgb(51, 51, 51); font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, Oxygen, Ubuntu, Cantarell, &quot;Open Sans&quot;, &quot;Helvetica Neue&quot;, sans-serif; font-style: normal; font-variant-caps: normal; font-weight: normal; text-size-adjust: auto;\">Durante las Ãºltimas semanas ha cobrado fuerza el rumor de que<span class=\"Apple-converted-space\">&nbsp;</span><span style=\"font-weight: 700;\">Microsoft</span>estarÃ­a trabajando en<span class=\"Apple-converted-space\">&nbsp;</span><a href=\"https://www.vidaextra.com/nintendo-switch/xbox-game-pass-microsoft-nintendo-switch-esto-que-dicen-rumores\" style=\"text-decoration-line: underline; color: rgb(0, 95, 159);\">la llegada de la marca Xbox a otras consolas</a>. Con Switch como principal objetivo, el rÃ­o de rumores no sÃ³lo parece factible, tambiÃ©n podrÃ­a ser la respuesta a<span class=\"Apple-converted-space\">&nbsp;</span><span style=\"font-weight: 700;\">un proyecto mucho mÃ¡s ambicioso e inteligente</span>.</p><p style=\"margin-right: auto; margin-bottom: 17.68px; margin-left: auto; font-size: 19px; max-width: 696px; caret-color: rgb(51, 51, 51); color: rgb(51, 51, 51); font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, Oxygen, Ubuntu, Cantarell, &quot;Open Sans&quot;, &quot;Helvetica Neue&quot;, sans-serif; font-style: normal; font-variant-caps: normal; font-weight: normal; text-size-adjust: auto;\">Lo aparentemente absurdo de la situaciÃ³n, â€œ<em style=\"font-style: italic;\">regalar</em>â€ a la competencia su mejor baza, no ha tardado en despertar quejas y muecas de desaprobaciÃ³n. Pero si analizamos la trayectoria de<span class=\"Apple-converted-space\">&nbsp;</span><span style=\"font-weight: 700;\">Xbox</span><span class=\"Apple-converted-space\">&nbsp;</span>durante los Ãºltimos aÃ±os, es fÃ¡cil ver cÃ³mo el rumor se convierta en algo factible y la locura en una estrategia acertada.</p>\r\n                        ', 'http://localhost/blog/view/image/noticias/lleno/post/51CO_2BY4bdPL_1024x1024.jpg', '2019-02-06', '2019-02-27', '2019-02-27'),
(66, 'lleno', '', 7, 'juan cagon', '', '\r\n            \r\n            \r\n            \r\n            \r\n            \r\n            \r\n            \r\n            \r\n            Juan eres un mierder hijo de la chingada&nbsp;                                                ', 'http://localhost/blog/view/image/noticias/lleno/post/1537195975-85667-zoom-500x600.jpg', '2019-02-14', '2019-02-25', '0000-00-00'),
(67, 'lleno', 'editor', 5, 'Parapa de rapper', '', '\r\n            Tengo un problema cuando guardo cosas copiadas por ahi, tengo que resolver lo del html            ', 'http://localhost/blog/view/image/noticias/lleno/post/1541973625-140024-zoom-450x540.jpg', '2019-02-14', '2019-03-06', '0000-00-00'),
(68, 'lleno', 'editor', 9, 'Nueva publicacion con Transaction', '', 'asdasd oiajsdoi', 'http://localhost/blog/view/image/noticias/lleno/post/1366_2000.jpg', '2019-02-24', '2019-02-28', '0000-00-00'),
(90, 'llena', '', 7, 'Array de titulo', 'ja ajaja', '\r\n            Habia una vez un circo que me volaba por ahi            ', 'http://localhost/blog/view/image/noticias/llena/post/EdDNhhLULXC50iof3p088H4ERKSZkp-iOHSehaekxZs.jpg', '2019-02-27', '2019-02-28', '0000-00-00'),
(91, 'lleno', '', 6, 'IndignaciÃ³n en las aulas', '', 'Habia una vez un grupo de jovenes que decidieron ponerse a estudiar un grado superior de desarrollo de aplicaciones web&nbsp;', 'http://localhost/blog/view/image/noticias/lleno/post/1541973625-140024-zoom-450x540.jpg', '2019-03-06', '0000-00-00', '0000-00-00'),
(92, 'lleno', 'editor', 5, 'Vamos a probar', 'son toods', '\r\n            oiasjd oihasdouhas oiuhasd oiuhad            ', 'http://localhost/blog/view/image/noticias/lleno/post/cartel-14n.jpg', '2019-03-07', '2019-03-07', '2019-03-07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seccion`
--

CREATE TABLE `seccion` (
  `id_seccion` int(11) NOT NULL,
  `descripcion` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `seccion`
--

INSERT INTO `seccion` (`id_seccion`, `descripcion`) VALUES
(1, 'Deportes'),
(2, 'Economia'),
(3, 'Videojuegos'),
(4, 'eSports'),
(5, 'Politica'),
(6, 'Tecnologia'),
(7, 'Cine'),
(8, 'Musica'),
(9, 'General');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_usuario`
--

CREATE TABLE `tipo_usuario` (
  `id_tipousuario` int(11) NOT NULL,
  `tipousuario` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_usuario`
--

INSERT INTO `tipo_usuario` (`id_tipousuario`, `tipousuario`) VALUES
(1, 'Periodista'),
(2, 'Editor'),
(3, 'Administrador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `id_tipousuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`username`, `password`, `nombre`, `id_tipousuario`) VALUES
('admin', '4321', 'Administrador', 3),
('editor', '1234', 'El editor', 2),
('llena', '1234', 'Llena', 1),
('lleno', '1234', 'Enric', 1),
('llenon', '1234', 'eric', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `keywords`
--
ALTER TABLE `keywords`
  ADD PRIMARY KEY (`id_keywords`),
  ADD UNIQUE KEY `keyword` (`keyword`);

--
-- Indices de la tabla `keywordsNews`
--
ALTER TABLE `keywordsNews`
  ADD PRIMARY KEY (`id_noticia`,`id_keyword`),
  ADD KEY `cascadeDElete` (`id_keyword`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id_noticia`),
  ADD KEY `id_noticia` (`id_noticia`);

--
-- Indices de la tabla `seccion`
--
ALTER TABLE `seccion`
  ADD PRIMARY KEY (`id_seccion`);

--
-- Indices de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  ADD PRIMARY KEY (`id_tipousuario`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`username`),
  ADD KEY `fk_id_tipousuario` (`id_tipousuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `keywords`
--
ALTER TABLE `keywords`
  MODIFY `id_keywords` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=264;

--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id_noticia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT de la tabla `seccion`
--
ALTER TABLE `seccion`
  MODIFY `id_seccion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  MODIFY `id_tipousuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `keywordsNews`
--
ALTER TABLE `keywordsNews`
  ADD CONSTRAINT `cascadeDElete` FOREIGN KEY (`id_keyword`) REFERENCES `keywords` (`id_keywords`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_id_keyword` FOREIGN KEY (`id_keyword`) REFERENCES `keywords` (`id_keywords`),
  ADD CONSTRAINT `fk_id_noticia` FOREIGN KEY (`id_noticia`) REFERENCES `noticias` (`id_noticia`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_id_tipousuario` FOREIGN KEY (`id_tipousuario`) REFERENCES `tipo_usuario` (`id_tipousuario`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
